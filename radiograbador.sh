#!/bin/bash

BASEDIR=$(dirname $0)

DATE=$(date +"%d%m%Y")

FILENAME="filename_$DATE.mp3" # filename

FOLDER="$BASEDIR/output" # folder

OUTPUT="$FOLDER/$FILENAME"

TMP="/tmp/$FILENAME"

LOG="$BASEDIR/last.log" 

URL="http://urlstream" # url stream

TIMEOUT="120m" # time record ('s' for seconds (the default), 'm' for minutes, 'h' for hours or 'd' for days.) 

T_BOT_TOKEN="1111111111:XXXxx1xX1X1-xXXXX1X1xXxxxX1xxXXX1xx" # token telegram bot

T_CHANNEL_ID="-1111111111111" # id telegram channel


# mkdir output

mkdir -p $FOLDER

# Record

timeout $TIMEOUT curl --output $OUTPUT $URL > $LOG 2>&1

# Compress 

lame --mp3input -b32 $OUTPUT $TMP >> $LOG 2>&1

# Telegram

curl -F document=@"$OUTPUT" https://api.telegram.org/bot$T_BOT_TOKEN/sendDocument?chat_id=$T_CHANNEL_ID >> $LOG # 2>&1

# Clean 

rm $TMP
